import os

from flask import Blueprint, jsonify, request
from redis import Redis
from protect import verify

test_bp = Blueprint('test', __name__)

r = Redis(host=os.environ['UPSTASH_URL'], port=41104, password=os.environ['UPSTASH_PASS'])


@test_bp.route('/env')
def env():
    verify(request)
    return jsonify({'res': 'OK', 'message': os.environ['FOO']})


@test_bp.route('/redis/set/<string:key>', methods=['POST'])
def redis_set(key):
    r.json().set(key, '$', request.json)
    return f'OK:set:{key}'


@test_bp.route('/redis/get/<string:key>')
def redis_get(key):
    if r.exists(key):
        return jsonify(r.json().get(key, '$'))
    else:
        return jsonify({'error': f'Key {key} not found'}), 206
