import os

from flask import Blueprint, jsonify
from langchain.chains import LLMChain
from langchain_community.chat_models import ChatOpenAI
from langchain_core.prompts import PromptTemplate
from redis import Redis
from tools import proxycurl

lc_linkedin_bp = Blueprint('lc_linkedin', __name__)

r = Redis(host=os.environ['UPSTASH_URL'], port=41104, password=os.environ['UPSTASH_PASS'])


@lc_linkedin_bp.route('/person/<string:username>')
def proxy_linkedin(username):
    key = f'person:{username}'
    if r.exists(key):
        return r.json().get(key, '$')[0]
    else:
        response = proxycurl.get_user_profile(username)
        r.json().set(key, '$', response)
        return response if response else {'error': 'Not found'}, 206


@lc_linkedin_bp.route('/person/summary/<string:username>')
def langchain_generate(username):
    key = f'person:{username}'
    if not r.exists(key):
        return jsonify({'error': f'Key {key} not found, please generate it first'}), 206

    profile = r.json().get(key, '$')[0]

    llm = ChatOpenAI(openai_api_key=os.environ['OPENAI_API_KEY'],
                     temperature=0,
                     model_name="gpt-3.5-turbo-0125")

    summary_template = """
        Given the object {profile} about a person I want you to create:
        1. A short summary
        2. two interesting facts about them
        
        Return only point 1 and 2
        """

    summary_prompt_template = PromptTemplate(
        input_variables=["profile"], template=summary_template
    )

    chain = LLMChain(llm=llm, prompt=summary_prompt_template)
    res = chain.invoke(input={"profile": profile})
    return jsonify(res.get('text'))
