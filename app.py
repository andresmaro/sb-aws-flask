from flask import Flask, jsonify, make_response, request

from protect import verify
from routes import lc_linkedin_bp, test_bp

app = Flask(__name__)
app.register_blueprint(test_bp, url_prefix='/test')
app.register_blueprint(lc_linkedin_bp, url_prefix='/linkedin')


@app.route("/")
def hello_from_root():
    return jsonify(message='Sandbox working!')


@app.errorhandler(404)
def resource_not_found(e):
    return make_response(jsonify(error='Not found!'), 404)
