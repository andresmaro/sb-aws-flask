-include .env

install-flask-requirements:
	@echo "Installing Flask requirements in $(FLASK_LAYER_DIR)/python/lib/python3.11/site-packages"
	rm -rf $(FLASK_LAYER_DIR)/python
	pip3.11 install -r $(FLASK_LAYER_DIR)/requirements.txt --target $(FLASK_LAYER_DIR)/python/lib/python3.11/site-packages

install-Langchain-requirements:
	@echo "Installing Langchain requirements in $(LANGCHAIN_LAYER_DIR)/python/lib/python3.11/site-packages"
	rm -rf $(LANGCHAIN_LAYER_DIR)/python
	pip3.11 install -r $(LANGCHAIN_LAYER_DIR)/requirements.txt --target $(LANGCHAIN_LAYER_DIR)/python/lib/python3.11/site-packages

install-requirements:
	pip freeze | xargs pip uninstall -y
	@echo "Packages removed"
	pip install -r $(FLASK_LAYER_DIR)/requirements.txt
	pip install -r $(LANGCHAIN_LAYER_DIR)/requirements.txt