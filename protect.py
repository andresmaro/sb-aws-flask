import os
from flask import abort


def verify(req):
    token = req.headers.get('Authorization')
    if token == f"Bearer {os.environ['SECRET']}":
        return True
    else:
        abort(401, description="Unauthorized")
