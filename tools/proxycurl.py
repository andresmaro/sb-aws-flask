import os

import requests


def get_user_profile(username):
    api_endpoint = 'https://nubela.co/proxycurl/api/v2/linkedin'
    headers = {'Authorization': f'Bearer {os.environ["PROXYCURL_API_KEY"]}'}
    params = {
        'linkedin_profile_url': f'https://www.linkedin.com/in/{username}',
    }
    response = requests.get(api_endpoint, params=params, headers=headers)

    data = response.json()
    data = {
        k: v
        for k, v in data.items()
        if v not in ([], "", None)
           and k not in ["people_also_viewed", "certifications"]
    }
    if data.get("groups"):
        for group_dict in data.get("groups"):
            group_dict.pop("profile_pic_url", None)

    if response.status_code != 200:
        return {}

    return data
